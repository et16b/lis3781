> **NOTE:** This README.md file should be placed at the **root of each 
of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project 
documentation as per below--otherwise, points **will** be deducted.
>

# lis3781

## Eli Torres

### A2 # Requirements:

*Sub-Heading:*

1. Descriptions
2. Screenshots
3. BitBucket tutorial

#### README.md file should include the following items:

* Git statements descriptions
* Screenshots of code
* Screenshots of query results
* Link to BitBucketStationsLocations tutorial

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates a new repository
2. git status - displays state of working directory and staging area
3. git add - moves changes from the working directory to the Git staging area
4. git commit - takes the staged snapshot and commits it to the project history
5. git push - used to upload local repository content to remote repository
6. git pull - used to fetch and download content from remote repository and update local repository to match that content
7. git branch - list branches

#### Assignment Screenshots:

*Screenshot of sql code:

![SQL CODE A Screenshot](img/lis3781_a2_sql_code_a.PNG)

![SQL CODE B Screenshot](img/lis3781_a2_sql_code_b.PNG)

*Screenshot of populated tables:

![populated tables screenshot](img/lis3781_a2_populated_tables.PNG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial 
Link](https://bitbucket.org/et16b/bitbucketstationlocations/
"Bitbucket Station Locations")

*Lis3781 Repo link:*
[A1 Lis3781 repo 
Link](https://bitbucket.org/et16b/lis3781/)
