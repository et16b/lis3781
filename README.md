> **NOTE:** This README.md file should be placed at the **root of each 
of your main directory.**

# lis3781 - Advance Database Management

## Eli Torres

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Amps
    - Create ERD of database
    - Create databse for HR department 

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Bitbucket repo
    - Complete BitBucket tutorial
    - Provide git command descriptions
    - Screenshots of sql code and query results

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Log into and work with Oracle SQL Developer
    - Create and populate tables
    - Make README file and put into bitbucket

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Design a database in order to track and document a city's court 
      case data
    - Create ERD of database
    - Salt and Hash social security numbers
5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Develop a database in order to track day-to-day business operations. 
    - Needs an updated method for storing data, running reports, and making business 
      decisions based upon trends and forecasts, as well as maintaining historical data
    - Microsoft SQL Server
    - Salt and Hash social security numbers

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Building upon the last assignment 
    - Create small data mart as a test platform
    - This involves creating a more detailed sales report
    - Also expand on more characteristics for location

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Install MongoDB
    - Import data into database
    - Creating queries to search, update, and remove data

