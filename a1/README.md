> **NOTE:** This README.md file should be placed at the **root of each 
of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project 
documentation as per below--otherwise, points **will** be deducted.
>

# lis3781

## Eli Torres

### A1 # Requirements:

*Sub-Heading:*

1. ERD
2. Screenshots code

#### README.md file should include the following items:

* Ampps Installation
* ERD
* SQL CODE

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Assignment Screenshots:

*Screenshot of ERD:

![ERD Screenshot](img/ERD.PNG)

*Screenshot of code:

![SQL code screenshot](img/A1_SQL_CODE.PNG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial 
Link](https://bitbucket.org/et16b/bitbucketstationlocations/
"Bitbucket Station Locations")

*Lis3781 Repo link:*
[A1 Lis3781 repo 
Link](https://bitbucket.org/et16b/lis3781/)
