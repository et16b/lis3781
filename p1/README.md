> **NOTE:** This README.md file should be placed at the **root of each 
of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project 
documentation as per below--otherwise, points **will** be deducted.
>

# lis3781

## Eli Torres

### P1 # Requirements:

*Sub-Heading:*

1. Descriptions
2. Screenshots
3. Links

#### README.md file should include the following items:

* Screenshots of ERD database
* Screenshots of code and populated tables
* lis3781 bitbucket repo link

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of ED:

![ERD Screenshot](img/P1_ERD.png)

*Screenshot of sql code:

![SQL CODE A Screenshot](img/P1_code_a.PNG)

![SQL CODE B Screenshot](img/P1_code_b.PNG)

*Screenshot of populated tables:

![populated tables screenshot](img/P1_results.PNG)


#### Links:

*Lis3781 Repo link:*
[Lis3781 repo 
Link](https://bitbucket.org/et16b/lis3781/)
